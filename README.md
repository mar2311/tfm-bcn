
# FINAL MASTER THESIS CODE:
## Study on the influence of addiyng Radiomic features to a traines Inception-ResNet-v2 model
-----------------------------------------------------------------------------------------
### Introduction:
This is the code used for the Final Master Thesis of Mar Hernández Obiols. 
The project aimed to study the influence of addiyng manually extracted features
(also referred as Radiomics) into a trained deep learning model (specifically, 
an Inception-ResNet-v2 model). 

The way to study this effect in a versatile way that could be easily addapt to 
any change in the number of types of Radiomic features was the following one:

1. Feature extraction using the deep learning model. From now on, these features 
will be refered as deep learning features
2. Extraction of Radiomic features. This extraction is performed trhough the 
texture analysis using wavelet statistics to extract a set of first-order statistics
and second-order statistics (co-occurrence matrix statistics). Finally, these features
are scaled using a Robust Scaler. From now on, these features will be refered as 
Radiomic features.
3. Concatenation of both types of features and classification into 4 different
classes using a simple dense layer and a droput out layer. 

The deep learning feature extraction wwas based on the model described by Kwok et al.
from 2018, and the Radiomic features extraction on the Niwas et al. paper from 2013.

--------------------------------------------------------------------------------------

### Instructions to run the code:

The code starts by running the main.py script. 

The main code is stored inside the folder tfm (Thesis Final Master). This folder 
contains the scripts:

* _init_py - Python script. 
* features.py - Python script. Used to extract the Radiomic features
* model.py - Python script (Keras). Contains three different classes: 
1. KwokModel() - Trains the Inception-Resnet-v2 model using k-fold cross
validation; extracts the deep-learning features; and predicts the test set
2. DenseKwokFeatures() - Trains the final classifier model using k-fold cross
validation; and predicts the test set
3. MetaModel() - Tests different rules and strategies to know which classifier
strategy is better under different circumstances (either only using deep-learning 
features or concatenating deep-learning with Radiomic features)
* preprocess.py - Python script (Pandas). All the preprocessing used in
the different main processes detailed in this block. The proprocess refers either to 
image enhancement for the wavelet analysis, patch obtantion, and table data analysis. 
* wavelets.m - Matlab script. Used to apply the desired Wavelet transformation (Complex
Daubechies) with the specified wavelet coefficients. 