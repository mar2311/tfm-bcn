import glob
import logging
import os
import re
import sys

import cv2
import matplotlib.image as mpimg
import numpy as np
import pandas as pd
from keras import optimizers
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard
from keras.layers import Dense, Dropout, GlobalAveragePooling2D
from keras.models import Model, Sequential
from keras.preprocessing.image import ImageDataGenerator
from skimage.io import imsave
from skimage.util.shape import view_as_windows
from sklearn.model_selection import StratifiedKFold, train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing

ROOT_PATH = os.path.join(os.path.dirname(__file__), '..')
CLASSES = ['Benign','Normal','InSitu','Invasive']

# Logger
fh = logging.FileHandler(os.path.join(ROOT_PATH, 'logs/tfm_log.log'))
fh.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
LOG = logging.getLogger('tfm_log_info')
LOG.setLevel(logging.DEBUG)
LOG.addHandler(fh)
del fh
