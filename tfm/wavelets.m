clear all;
close all;
addpath('D:/Projects/thesissjtu/Data/ICIAR2018_BACH_Challenge/Patches_no_resize')
S=dir('D:/Projects/thesissjtu/Data/ICIAR2018_BACH_Challenge/Patches_no_resize');

LoD = [0.0105+0.0206i, -0.0171+0.0087i, -0.0806-0.1179i,  0.1514-0.0942i, 0.6430+0.1829i, 0.6430+0.1829i, 0.1514-0.0942i, -0.0806-0.1179i, -0.0171+0.0087i, 0.0105+0.0206i];
HiD = [0.0105+0.0206i, 0.0171-0.0087i, -0.0806-0.1179i, -0.1514+0.0942i, 0.6430+0.1829i, -0.6430-0.1829i, 0.1514-0.0942i, 0.0806+0.1179i, -0.0171+0.0087i, -0.0105-0.0206i];


for i=3:length(S)
    addpath(fullfile('D:/Projects/thesissjtu/Data/ICIAR2018_BACH_Challenge/Patches_no_resize/',S(i).name));
    S2 = dir(fullfile('D:/Projects/thesissjtu/Data/ICIAR2018_BACH_Challenge/Patches_no_resize/',S(i).name));
    for j=3:length(S2)
        disp(S2(j).name)
        im=imread(fullfile(S2(1).folder,S2(j).name));
        im=rgb2hsv(im);
        v_im = im(:,:,3);
        strat_v=(v_im-min(v_im(:)))/(max(v_im(:))-min(v_im(:)));
        im(:,:,3) = strat_v;
        im = hsv2rgb(im);
        im = rgb2gray(im);
        % First wavelet level
        disp('1x')
        [cA,cH,cV,cD] = dwt2(im,LoD,HiD,'mode','symh');
        path=fullfile('data/daubechies/predaubechies/','1x',S(i).name,S2(j).name(1:end-4));
        mkdir(path)
        csvwrite(fullfile(path,'cA.csv'),cA)
        csvwrite(fullfile(path,'cH.csv'),cH)
        csvwrite(fullfile(path,'cV.csv'),cV)
        csvwrite(fullfile(path,'cD.csv'),cD)
        
        % Second wavelet level
        disp('2x')
        [cA,cH,cV,cD] = dwt2(cA,LoD,HiD,'mode','symh');
        path=fullfile('data/daubechies/predaubechies/','2x',S(i).name,S2(j).name(1:end-4));
        mkdir(path)
        csvwrite(fullfile(path,'cA.csv'),cA)
        csvwrite(fullfile(path,'cH.csv'),cH)
        csvwrite(fullfile(path,'cV.csv'),cV)
        csvwrite(fullfile(path,'cD.csv'),cD)
        
        % Third wavelet level
        disp('3x')
        [cA,cH,cV,cD] = dwt2(cA,LoD,HiD,'mode','symh');
        path=fullfile('data/daubechies/predaubechies/','3x',S(i).name,S2(j).name(1:end-4));
        mkdir(path)
        csvwrite(fullfile(path,'cA.csv'),cA)
        csvwrite(fullfile(path,'cH.csv'),cH)
        csvwrite(fullfile(path,'cV.csv'),cV)
        csvwrite(fullfile(path,'cD.csv'),cD)
    end
end




