#%%
import matplotlib.pyplot as plt
import numpy as np
from skimage import exposure, feature
import os
import glob
import pandas as pd
from pandas.api.types import CategoricalDtype
import numpy.matlib
# from radiomics import featureextractor
from skimage.util.shape import view_as_windows
from multiprocessing import Pool

#%%
def prepare_images_for_radiomics():
    # dbwave_path = "data/daubechieswavelet"
    # filename = os.listdir(dbwave_path)[0]
    # M, N, limit = 772, 1028, 0.1
    # clas = os.listdir('/'.join([dbwave_path, filename]))[0]
    # image = os.listdir('/'.join([dbwave_path, filename, clas]))[0]
    # wavelet = os.listdir('/'.join([dbwave_path, filename, clas, image]))[1]
    # path = '/'.join([dbwave_path, filename, clas, image, wavelet])
    # new_data= np.loadtxt(path, 
    #                         delimiter=',', dtype=np.complex)
    # im=np.reshape(new_data, (M, N)).real
    # im = 
    re_all_mins=np.array([])
    re_all_maxs=np.array([])
    im_all_mins=np.array([])
    im_all_maxs=np.array([])
    dbwave_path = "data/daubechieswavelet"
    for filename in os.listdir(dbwave_path):
        print(filename)
        re_resolution_mins=[]
        re_resolution_maxs=[]
        im_resolution_mins=[]
        im_resolution_maxs=[]
        if filename == '1x':
            M, N, limit = 772, 1028, 0.1

        if filename == '2x':
            M, N, limit = 390, 518, 0.25

        if filename == '3x':
            M, N, limit = 199, 263, 1

        for clas in os.listdir('/'.join([dbwave_path, filename])):
            print(clas)
            for image in os.listdir('/'.join([dbwave_path, filename, clas])):
                for wavelet in os.listdir('/'.join([dbwave_path, filename, clas, image])):
                    if (wavelet[0:2]=='cA'):
                        continue
                    path = '/'.join([dbwave_path, filename, clas, image, wavelet])
                    new_data= np.loadtxt(''.join([path[0:-4], '.txt']), 
                                            delimiter=',', dtype=np.complex)
                    im=np.reshape(new_data, (M, N))
                    re_resolution_mins.append(np.min(im.real))
                    re_resolution_maxs.append(np.max(im.real))
                    im_resolution_mins.append(np.min(im.imag))
                    im_resolution_maxs.append(np.max(im.imag))
        re_resolution_mins = np.array(re_resolution_mins)
        re_resolution_maxs = np.array(re_resolution_maxs)
        im_resolution_mins = np.array(im_resolution_mins)
        im_resolution_maxs = np.array(im_resolution_maxs)
        re_all_mins = np.vstack([re_all_mins, re_resolution_mins]) if re_all_mins.size else re_resolution_mins
        re_all_maxs = np.vstack([re_all_maxs, re_resolution_maxs]) if re_all_maxs.size else re_resolution_maxs
        im_all_mins = np.vstack([im_all_mins, im_resolution_mins]) if im_all_mins.size else im_resolution_mins
        im_all_maxs = np.vstack([im_all_maxs, im_resolution_maxs]) if im_all_maxs.size else im_resolution_maxs

    # featureVector = extractor.execute(imageName, maskName)

def generate_first_histograms():

    real_data, imag_data = generate_distribution()
    plt.figure()
    plt.hist(df.loc[(df.clase=='Benign') & (df.tipo_wavelet=='H') & (df.resolucion=='1x'),'real'], 
             bins=50, rwidth=0.95)
    df.loc[df.resolucion=='3x', 'real'].hist(by=[df.clase, df.tipo_wavelet])
    plt.show()


	

def generate_distribution():
    files = glob.glob('data/daubechieswavelet/*/*/*/*.txt')
    df_paths = pd.DataFrame(data={'path':files})
    df_paths['resolucion'] = [x.split("\\")[1] for x in df_paths.path.values]
    df_paths['clase'] = [x.split("\\")[2] for x in df_paths.path.values]
    df_paths['num_img'] = [int(x.split("\\")[3][-3:]) for x in df_paths.path.values]
    df_paths['tipo_wavelet'] = [x.split("\\")[4][1] for x in df_paths.path.values]
    df_paths = df_paths.sort_values(['num_img'])

    num_img = 1
    df_imgs = pd.DataFrame()
    cats_res = CategoricalDtype(categories=df_paths.resolucion.unique())
    cats_clase = CategoricalDtype(categories=df_paths.clase.unique())
    cats_tw = CategoricalDtype(categories=df_paths.tipo_wavelet.unique())
    for row in df_paths.itertuples(index=False):
        print(row)
        if(row.num_img != num_img):
            df_imgs.to_csv('data/daubechieswavelet/df_imgs.csv')
            num_img = row.num_img
        new_data = np.loadtxt(row.path, delimiter=',', dtype=np.complex)
        df_img = pd.DataFrame(data={
            'real': new_data.real.flatten(),
            'imag': new_data.imag.flatten(),
            'resolucion': row.resolucion,
            'clase': row.clase,
            'tipo_wavelet': row.tipo_wavelet
            })
        df_img['real'] = df_img.real.astype(np.float32)
        df_img['imag'] = df_img.imag.astype(np.float32)
        df_img['resolucion'] = df_img.resolucion.astype(cats_res)
        df_img['clase'] = df_img.clase.astype(cats_clase)
        df_img['tipo_wavelet'] = df_img.tipo_wavelet.astype(cats_tw)
        df_imgs = df_imgs.append(df_img)

    return df_imgs



def contrast_stretching(im):
    # Contrast stretching
    p2 = np.percentile(im, 2)
    p98 = np.percentile(im, 98)
    img_rescale = exposure.rescale_intensity(im, in_range = (p2, p98))

    return img_rescale

def create_isaac_features(im):
    f1 = im.mean()
    f2 = im.std()
    f3 = np.sum(im - f1**3) / im.size  # Skewness
    f4 = np.max(im)
    f5 = np.min(im)
    wav_stat_features = np.array([[f1], [f2], [f3], [f4], [f5]])
    wav_stat_features

    comatrix = feature.greycomatrix(
        np.uint8(im), [3], [0, np.pi/4, np.pi/2, np.pi*3/4], normed=True)
    
    n_bins = 255 - 0 + 1
    indexdiff = np.zeros((n_bins, n_bins))
    matrixi = np.zeros((n_bins, n_bins))
    for i in range(n_bins): 
        indexdiff[i,i:] = np.arange(0, n_bins-i, 1)
        matrixi[i,:] = np.arange(0, n_bins, 1)

    indexdiff = indexdiff + indexdiff.T
    squ_idxdiff = np.square(indexdiff)
    
    win_size = 32
    stride = 1
    
    squidxdiff_split = view_as_windows(squ_idxdiff, (win_size,win_size),stride)
    matrixi_split = view_as_windows(matrixi, (win_size,win_size),stride)

    # Contrast, energy, entropy and local homogeneity
    contrast, energy, entropy, lochomo, maxprob = [], [], [], [], []
    clshade, clprom, infocorr = [], [], []

    for i in range(4): #[0,np.pi/4,np.pi/2,np.pi*3/4]
        comat_i = comatrix[:,:,0,i]
                
        comat_split = view_as_windows(comat_i, (win_size,win_size),stride)

        all_contrast, all_energy, all_entropy, all_lochomo, all_maxprob = [], [], [], [], []
        all_clshade, all_clprom, all_infocorr = [], [], []

        for j in range(np.shape(comat_split)[0]):
            for k in range(np.shape(comat_split)[1]):
                sub_co_matix = comat_split[j,k,:,:]
                mean_val = np.mean(sub_co_matix)
                if mean_val ==0:
                    continue
                sub_sq_idx_matix = squidxdiff_split[j,k,:,:]
                sub_matx_matixi = matrixi_split[j,k,:,:]

                all_contrast.append(np.sum(sub_sq_idx_matix * sub_co_matix))
                all_energy.append(np.sum(np.square(sub_co_matix)))
                all_entropy.append(-np.sum(np.nan_to_num(sub_co_matix * np.log(sub_co_matix))))
                all_lochomo.append(-np.sum(1 / (1 + sub_sq_idx_matix) * sub_co_matix))
                all_maxprob.append(np.amax(sub_co_matix))

                Mx = np.sum(sub_matx_matixi * sub_co_matix)
                My = np.sum(sub_matx_matixi.T * sub_co_matix)

                all_clshade.append(np.sum(((sub_matx_matixi - Mx + sub_matx_matixi.T - My)**3) * sub_co_matix))
                all_clprom.append(np.sum(((sub_matx_matixi - Mx + sub_matx_matixi.T - My)**4) * sub_co_matix))

                Sx = np.sum(sub_co_matix, axis=1)
                Sy = np.sum(sub_co_matix, axis=0)
                Sxy = Sx.reshape(-1, 1).dot(Sy.reshape(1, -1))
                Hxy = -np.sum(np.nan_to_num(sub_co_matix * np.log(Sxy)))

                all_infocorr.append((all_entropy[-1] - Hxy) / np.max(np.array([np.max(Sx), np.max(Sy)])))
                
        contrast.append(np.mean(all_contrast))
        energy.append(np.mean(all_energy))
        entropy.append(np.mean(all_entropy))
        lochomo.append(np.mean(all_lochomo))
        maxprob.append(np.mean(all_maxprob))

        clshade.append(np.mean(all_clshade))
        clprom.append(np.mean(all_clprom))
        infocorr.append(np.mean(all_infocorr))

    wav_co_features = np.array([contrast, energy, entropy, lochomo, maxprob, 
                                clshade, clprom, infocorr])

    return np.abs(wav_stat_features), np.abs(wav_co_features)

def main_image(dbwave_path, path_save, clas, image):
    print(image)
    for filename in os.listdir('/'.join([dbwave_path, clas, image])):
        if filename == '1x':
            #M, N, limit = 772, 1028, 0.1
            M, N = 752, 752

        if filename == '2x':
            #M, N, limit = 390, 518, 0.25
            M, N = 380, 380

        if filename == '3x':
            #M, N, limit = 199, 263, 1
            M, N = 194, 194

        # print(image[1:])
        # print(filename)
        # print(clas)

        re_isaac_wav_stat_features, re_isaac_wav_co_features = [],[]
        im_isaac_wav_stat_features, im_isaac_wav_co_features = [],[]

        for wavelet in os.listdir('/'.join([dbwave_path, clas, image, filename])):
            if (wavelet[0:2]=='cA'):
                continue
            os.makedirs('/'.join([path_save, clas, image, filename, wavelet[0:2]]), exist_ok=True)
            path = '/'.join([dbwave_path, clas, image, filename, wavelet])
            new_data= np.loadtxt(''.join([path[0:-4], '.txt']), 
                                    delimiter=';', dtype=np.complex)
            im=np.reshape(new_data, (M, N))

            # Real part:
            re_wav_stat_features, re_wav_co_features = create_isaac_features(im.real) #separa real and imaginary part

            # Imaginary part:
            im_wav_stat_features, im_wav_co_features = create_isaac_features(im.imag) #separa real and imaginary part

            # Nuclei texture variability:
            I = np.sqrt(np.square(im.real) + np.square(im.imag))
            J = np.gradient(np.arctan(np.nan_to_num(im.real / im.imag)))

            mean_I = np.sum(I,axis=1) / M
            extended_mean_I = np.matlib.repmat(mean_I, N, 1)
            rest_I = np.square(I) - np.square(extended_mean_I.T)
            std_I = np.sum(rest_I, axis = 1) / M

            mean_J_1 = np.sum(J[0][:][:], axis=1) / M
            extended_mean_J_1 = np.matlib.repmat(mean_J_1, N, 1)
            rest_J_1 = np.square(J[0][:][:]) - np.square(extended_mean_J_1.T)
            std_J_1 = np.sum(rest_J_1, axis = 1) / M

            mean_J_2 = np.sum(J[1][:][:], axis=1) / M
            extended_mean_J_2 = np.matlib.repmat(mean_J_2, N, 1)
            rest_J_2 = np.square(J[1][:][:]) - np.square(extended_mean_J_2.T)
            std_J_2 = np.sum(rest_J_2, axis = 1) / M

            lambdaI = np.sum(np.nan_to_num(mean_I / std_I)) / N
            lambdaJ_1 = np.sum(np.nan_to_num(mean_J_1 / std_J_1)) / N
            lambdaJ_2 = np.sum(np.nan_to_num(mean_J_2 / std_J_2)) / N

            f12_1 = lambdaI + lambdaJ_1
            f12_2 = lambdaI + lambdaJ_2
            f12 = [[f12_1], [f12_2]]

            path = '/'.join([path_save, clas, image, filename, wavelet[0:2]])
            os.makedirs(path, exist_ok=True)

            np.savetxt('/'.join([path, 're_isaac_wav_stat_features.txt']), 
                        re_wav_stat_features,fmt='%f')
            np.savetxt('/'.join([path, 're_isaac_wav_co_features.txt']),
                        re_wav_co_features,fmt='%f')
            np.savetxt('/'.join([path, 'im_isaac_wav_stat_features.txt']),
                        im_wav_stat_features,fmt='%f')
            np.savetxt('/'.join([path, 'im_isaac_wav_co_features.txt']),
                        im_wav_co_features,fmt='%f')
            np.savetxt('/'.join([path, 'isaac_texture_variability.txt']),
                        f12,fmt='%f')

def main():
    ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    dbwave_path = ROOT_PATH + "/data/daubechies/norm_daubechies"
    path_save = ROOT_PATH + '/data/isaac_features'
    for clas in os.listdir(dbwave_path):
        print(clas)
        with Pool(6) as pool:
            pool.starmap(
                main_image, 
                [(dbwave_path, path_save, clas, image) 
                for image in os.listdir('/'.join([dbwave_path, clas]))
                if len(glob.glob('/'.join([path_save, clas, image,'*/*/*.txt']))) < 45])
            

"""def copy():
    dbwave_path = "data/daubechies/norm_daubechies"
    for clas in os.listdir(dbwave_path):
        print(clas)

        for filename in os.listdir('/'.join([dbwave_path, clas])):
            if filename == '1x':
                #M, N, limit = 772, 1028, 0.1
                M, N = 772, 772

            if filename == '2x':
                #M, N, limit = 390, 518, 0.25
                M, N = 390, 390

            if filename == '3x':
                #M, N, limit = 199, 263, 1
                M, N = 199, 199

            if filename =='1x':
                if clas =='Benign':
                    path_save = 'data/isaac_features'
                    os.mkdir(path_save)
                os.mkdir('/'.join([path_save, clas]))

            print(clas)

            for image in os.listdir('/'.join([dbwave_path, clas, filename])):
                if filename == '1x':
                    os.mkdir('/'.join([path_save, clas, image]))
                os.mkdir('/'. join([path_save+clas, image, filename]))
                print(image)
                re_isaac_wav_stat_features, re_isaac_wav_co_features = [],[]
                im_isaac_wav_stat_features, im_isaac_wav_co_features = [],[]

                for wavelet in os.listdir('/'.join([dbwave_path, filename, clas, image])):"""

if __name__ == "__main__":
    main()




