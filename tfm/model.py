import glob
import logging
import os
import re
import sys

import cv2
import matplotlib.image as mpimg
import numpy as np
import pandas as pd
from keras import optimizers
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard
from keras.layers import Dense, Dropout, GlobalAveragePooling2D
from keras.models import Model, Sequential
from keras.preprocessing.image import ImageDataGenerator
from skimage.io import imsave
from skimage.util.shape import view_as_windows
from sklearn.model_selection import StratifiedKFold, train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing

ROOT_PATH = os.path.join(os.path.dirname(__file__), '..')
CLASSES = ['Benign','Normal','InSitu','Invasive']

# Logger
fh = logging.FileHandler(os.path.join(ROOT_PATH, 'logs/tfm_log.log'))
fh.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
LOG = logging.getLogger('tfm_log_info')
LOG.setLevel(logging.DEBUG)
LOG.addHandler(fh)
del fh

class KwokModel():

    def __init__(self):
        self.models = {}
        self.splits = self._get_splits()
        self.classes = ['Benign','Normal','InSitu','Invasive']

    def fit(self):
        LOG.info('Training kwok models...')
        idg = ImageDataGenerator(
            #other transformations available
            rotation_range=270,
            horizontal_flip=True,
            vertical_flip=True)
        for n_fold, split in enumerate(self.splits):
            df_train, df_val = split
            net = self._create_model()
            callbacks = self._create_callbacks(n_fold)
            print(df_train[['path'] + self.classes].head())
            print(df_val.head())
            train_gen = idg.flow_from_dataframe(
                df_train[['path'] + self.classes], x_col='path', y_col=self.classes,
                target_size=(299,299), batch_size=16, shuffle=True, class_mode="other"
            )
            val_gen = idg.flow_from_dataframe(
                df_val[['path'] + self.classes], x_col='path', y_col=self.classes,
                target_size=(299,299), batch_size=16, shuffle=True, class_mode="other"
            )
            net.fit_generator(
                train_gen, validation_data=val_gen, epochs=50,
                steps_per_epoch=250, validation_steps=250, callbacks=callbacks)
            self.models[n_fold] = net

    def predict_validation(self):
        LOG.info('Predicting kwok labels...')
        if len(self.models) == 0:
            LOG.debug('Loading models...')
            for n_fold in range(len(self.splits)):
                net = self._create_model()
                # Will fail if never fitted before
                net.load_weights(os.path.join(ROOT_PATH, f'models/kwok_m{n_fold}.h5'))
                self.models[n_fold] = net
        idg = ImageDataGenerator()
        df_res = pd.DataFrame()
        for n_fold, split in enumerate(self.splits):
            LOG.debug(f'Predicting split #{n_fold}...')
            df_val = split[1]
            val_gen = idg.flow_from_dataframe(
                df_val[['path'] + self.classes], x_col='path', y_col=self.classes,
                target_size=(299,299), batch_size=1, shuffle=False, class_mode="other"
            )
            preds = self.models[n_fold].predict_generator(
                val_gen, steps=len(df_val))
            df_val['pred'] = [self.classes[np.argmax(pred)] for pred in preds]
            for i, clase in enumerate(self.classes):
                df_val[f'pred_{clase.lower()}'] = [x[i] for x in preds]
            df_res = df_res.append(df_val)

        df_res = pd.melt(
            df_res, id_vars=['path','image','patch','pred'] + [f'pred_{x.lower()}' for x in self.classes],
            value_vars=self.classes, var_name='real')
        df_res = df_res.loc[df_res.value == 1].drop('value', axis=1)
        df_res.to_hdf(os.path.join(ROOT_PATH, 'results/pred_kwok.h5'), key='pred_kwok')
        return df_res

    def predict_last_layer(self):
        LOG.info('Predicting kwok last layer...')
        if len(self.models) == 0:
            for n_fold in range(len(self.splits)):
                net = self._create_model()
                # Will fail if never fitted before
                net.load_weights(os.path.join(ROOT_PATH, f'models/kwok_m{n_fold}.h5'))
                self.models[n_fold] = net
        idg = ImageDataGenerator()
        df_res = pd.DataFrame()
        for n_fold, split in enumerate(self.splits):
            df_val = split[1]
            val_gen = idg.flow_from_dataframe(
                df_val[['path'] + self.classes], x_col='path', y_col=self.classes,
                target_size=(299,299), batch_size=1, shuffle=False, class_mode="other"
            )
            model = self.models[n_fold]
            model.layers.pop()
            model.layers.pop()
            model = Model(input=model.input, output=model.layers[-1].output)
            preds = model.predict_generator(
                val_gen, steps=len(df_val))
            for i in range(2048):
                df_val[f'ft_{i}'] = [x[i] for x in preds]
            df_res = df_res.append(df_val)

        df_res = pd.melt(
            df_res, id_vars=['path','image','patch','filename'] + [f'ft_{x}' for x in range(2048)],
            value_vars=self.classes, var_name='real')
        df_res = df_res.loc[df_res.value == 1].drop('value', axis=1)
        df_res.to_hdf(os.path.join(ROOT_PATH, 'results/pred_last_layer.h5'), key='pred_last_layer')

    def predict_test(self):
        LOG.info('Predicting kwok labels...')
        if len(self.models) == 0:
            LOG.debug('Loading models...')
            for n_fold in range(len(self.splits)):
                net = self._create_model()
                # Will fail if never fitted before
                net.load_weights(os.path.join(ROOT_PATH, f'models/kwok_m{n_fold}.h5'))
                self.models[n_fold] = net
        idg = ImageDataGenerator()
        df_test = self._get_test()
        preds = np.zeros([len(df_test), 4])
        for n_fold, model in self.models.items():
            test_gen = idg.flow_from_dataframe(
                df_test[['path'] + self.classes], x_col='path', y_col=self.classes,
                target_size=(299,299), batch_size=1, shuffle=False, class_mode="other"
            )
            pred = self.models[n_fold].predict_generator(
                test_gen, steps=len(df_test))
            preds += pred
        preds /= len(self.models)
        df_test['pred'] = [self.classes[np.argmax(pred)] for pred in preds]
        for i, clase in enumerate(self.classes):
            df_test[f'pred_{clase.lower()}'] = [x[i] for x in preds]

        df_test = pd.melt(
            df_test, id_vars=['path','image','patch','pred'] + [f'pred_{x.lower()}' for x in self.classes],
            value_vars=self.classes, var_name='real')
        df_test = df_test.loc[df_test.value == 1].drop('value', axis=1)
        df_test.to_hdf(os.path.join(ROOT_PATH, 'results/test_kwok.h5'), key='test_kwok')
        return df_test

    def _create_model(self):
        # Define model
        base_model = InceptionResNetV2(include_top=False, weights='imagenet')

        # Remove last layers
        base_model.layers.pop()
        base_model.layers.pop()
        base_model.layers.pop()

        x = base_model.layers[-1].output
        x = GlobalAveragePooling2D()(x)
        # let's add a fully-connected layer
        x = Dense(2048, activation='relu')(x)
        x = Dropout(0.5)(x)
        # and a logistic layer -- let's say we have 200 classes
        predictions = Dense(4, activation='softmax')(x) #aunque usar una sigmoid aqui es raro --> multiclassification

        net = Model(inputs=base_model.input, outputs=predictions)

        ####sgd = optimizers.adam() # WHY decay??? = 1e-6
        sgd = optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
        net.compile(
            loss='categorical_crossentropy',
            optimizer=sgd,
            metrics=['categorical_accuracy']
        )

        return net

    def _create_callbacks(self, n_model):
        callbacks = [
            EarlyStopping(patience=10, verbose=1),
            ModelCheckpoint(
                os.path.join(ROOT_PATH, f'models/kwok_m{n_model}.h5'), verbose=1, save_best_only=True,
                save_weights_only=True, period=1),
            TensorBoard(
                log_dir=os.path.join(ROOT_PATH, f'logs/m{n_model}/'))
        ]
        return callbacks

    def _get_splits(self):
        # Create informed dataframe of paths
        df = pd.DataFrame()
        df['path'] = glob.glob(os.path.join(ROOT_PATH, 'data/patches/patches_part A/train/*/*'))
        df['path'] = [x.replace('\\', '/') for x in df['path']]
        df['class'] = [x.split('/')[-2] for x in df['path']]
        files = [x.split('/')[-1] for x in df['path']]
        df['image'] = [int(re.findall('\d+', x)[0]) for x in files]
        df['patch'] = [int(re.findall('\d+', x)[1]) for x in files]
        df['filename'] = [x.split('/')[-1].split('.')[0] for x in df['path']]

        # Kfold over the images (don't split the patches)
        df_image = df[['class','image']].drop_duplicates()
        kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=0)
        splits = list(kfold.split(X=df_image, y=df_image['class']))
        splits = [
            (pd.get_dummies(
                df.merge(df_image.iloc[split[0]], on=['class', 'image']),
                columns=['class'], prefix='', prefix_sep=''
            ),
            pd.get_dummies(
                df.merge(df_image.iloc[split[1]], on=['class', 'image']),
                columns=['class'], prefix='', prefix_sep=''
            ))
            for split in splits
        ]
        return splits

    def _get_test(self):
        # Create informed dataframe of paths
        df = pd.DataFrame()
        df['path'] = glob.glob(os.path.join(ROOT_PATH, 'data/patches/patches_part A/test/*/*'))
        df['path'] = [x.replace('\\', '/') for x in df['path']]
        df['class'] = [x.split('/')[-2] for x in df['path']]
        files = [x.split('/')[-1] for x in df['path']]
        df['image'] = [int(re.findall('\d+', x)[0]) for x in files]
        df['patch'] = [int(re.findall('\d+', x)[1]) for x in files]
        df['filename'] = [x.split('/')[-1].split('.')[0] for x in df['path']]

        df = pd.get_dummies(df, columns=['class'], prefix='', prefix_sep='')
        return df

class DenseKwokFeatures():

    def __init__(self):
        # Load last layer Kwok
        df_layer = pd.read_hdf(
            os.path.join(ROOT_PATH, 'results/pred_last_layer.h5'),
            'pred_last_layer')
        df_layer = df_layer.drop(['path', 'image', 'patch'], axis=1)
        df_layer = df_layer.rename(columns = {'filename':'image'})
        df_layer = df_layer.set_index('image')
        df_layer = df_layer.sort_index()

        # Load features
        robust_scaled_df = pd.read_csv(
        os.path.join(ROOT_PATH, 'data/tablas - features/normalized_final_tablas'),
            sep=';', index_col = 'image')
        robust_scaled_df.index = ['i'+x if x[0]=='s' or x[0]=='v' else x for x in robust_scaled_df.index]
        robust_scaled_df = robust_scaled_df.sort_index()
        robust_scaled_df = robust_scaled_df.drop(['real_class'], axis=1)

        all_tabla = pd.concat([df_layer, robust_scaled_df],axis=1,join="inner")
        all_tabla['image'] = [int(re.findall('\d+', x)[0]) for x in all_tabla.index]
        all_tabla['patch'] = [int(re.findall('\d+', x)[1]) for x in all_tabla.index]

        self.all_tabla = all_tabla
        self.splits = self._create_splits(all_tabla)
        self.models = {}


    def fit(self):
        LOG.info('Training kwok + features models...')
        for n_fold, split in enumerate(self.splits):
            df_train, df_val = split
            y_train = df_train[CLASSES]
            y_val = df_val[CLASSES]
            X_train = df_train.drop(CLASSES, axis=1)
            X_val = df_val.drop(CLASSES, axis=1)
            X_train = X_train.drop(['image','patch'], axis=1)
            X_val = X_val.drop(['image','patch'], axis=1)

            # Only for deep learning features
            # X_train = X_train.iloc[:,:2049]
            # X_val = X_val.iloc[:,:2049]

            # # Only for radiomics:
            X_train = X_train.iloc[:,2049:]
            X_val = X_val.iloc[:,2049:]

            net = self._create_model(np.shape(X_train)[1])
            callbacks = self._create_callbacks(n_fold)
            validation = list([X_val,y_val])
            net.fit(x=X_train, y=y_train,
                validation_data=validation, epochs=50, steps_per_epoch=250,
                validation_steps=250, callbacks=callbacks)
            self.models[n_fold] = net

    def predict(self):
        LOG.info('Predicting kwok + features labels...')
        df_res = pd.DataFrame()
        for n_fold, split in enumerate(self.splits):
            LOG.debug(f'Split #{n_fold}')
            df_train, df_val = split
            y_train = df_train[CLASSES]
            y_val = df_val[CLASSES]
            X_train = df_train.drop(CLASSES, axis=1)
            X_val = df_val.drop(CLASSES, axis=1)
            X_train = X_train.drop(['image','patch'], axis=1)
            X_val = X_val.drop(['image','patch'], axis=1)

            # Only for deep learning features
            # X_train = X_train.iloc[:,:2049]
            # X_val = X_val.iloc[:,:2049]

            # # Only for radiomics:
            X_train = X_train.iloc[:,2049:]
            X_val = X_val.iloc[:,2049:]

            net = self._create_model(np.shape(X_train)[1])
            net.load_weights(
                os.path.join(ROOT_PATH, f'models/meta_m{n_fold}.h5'))
            self.models[n_fold] = net
            preds = net.predict(X_val)
            df_val['pred'] = [CLASSES[np.argmax(pred)] for pred in preds]
            for i, clase in enumerate(CLASSES):
                df_val[f'pred_{clase.lower()}'] = [x[i] for x in preds]
            df_val['confidence'] = [np.max(pred) for pred in preds]
            df_res = df_res.append(df_val)

        df_res = pd.melt(
            df_res, id_vars=['image','patch','pred','confidence'] + [f'pred_{x.lower()}' for x in CLASSES],
            value_vars=CLASSES, var_name='real')
        df_res = df_res.loc[df_res.value == 1].drop('value', axis=1)
        df_res['correct'] = df_res.pred == df_res.real
        print((df_res.pred == df_res.real).mean())

        df_res.to_hdf(
            os.path.join(ROOT_PATH, 'results/pred_kwok_fts.h5'),
            'pred_kwok_fts')

    def _create_model(self, n_features):
        new_net = Sequential()
        # OPTION 1:
        # new_net.add(Dense(500, activation='relu', input_shape=(n_features,)))
        # new_net.add(Dropout(0.5))

        # OPTION 2:
        new_net.add(Dropout(0.5, input_shape=(n_features,)))

        #
        new_net.add(Dense(4, activation='softmax'))

        sgd = optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
        new_net.compile(
            loss='categorical_crossentropy',
            optimizer = sgd,
            metrics=['categorical_accuracy']
        )
        return new_net

    def _create_splits(self, all_tabla):
        df = all_tabla.copy()
        df_image = df[['image','real']].drop_duplicates()
        kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=0)
        splits = list(kfold.split(X=df_image, y=df_image['real']))
        splits = [
            (pd.get_dummies(
                df.merge(df_image.iloc[split[0]], on=['real', 'image']),
                columns=['real'], prefix='', prefix_sep=''
            ),
            pd.get_dummies(
                df.merge(df_image.iloc[split[1]], on=['real', 'image']),
                columns=['real'], prefix='', prefix_sep=''
            ))
            for split in splits
        ]
        return splits

    def _create_callbacks(self, n_model):
        callbacks = [
            EarlyStopping(patience=10, verbose=1),
            ModelCheckpoint(
                os.path.join(ROOT_PATH, f'models/meta_m{n_model}.h5'), verbose=1, save_best_only=True,
                save_weights_only=True, period=1),
            TensorBoard(
                log_dir=os.path.join(ROOT_PATH, f'logs/meta{n_model}/'))
        ]
        return callbacks


class MetaModel():

    def __init__(self):
        self.df = self._join_kw_dkwf()
        self.splits = self._create_splits(self.df)

    def _join_kw_dkwf(self):
        df_kwok = pd.read_hdf(os.path.join(ROOT_PATH, 'results/pred_kwok.h5'), 'pred_kwok')
        df_kwok['confidence'] = np.max(df_kwok[['pred_benign','pred_normal','pred_insitu','pred_invasive']].values, axis=1)

        df_dfwf = pd.read_hdf(os.path.join(ROOT_PATH, 'results/pred_kwok_fts.h5'), 'pred_kwok_fts')

        cols = ['image','patch', 'pred','confidence', 'real'] + [f'pred_{x.lower()}' for x in CLASSES]
        df = df_kwok[cols].merge(
            df_dfwf[cols], on=['image','patch','real'],suffixes=['_Kw','_Kw_ft'])
        return df

    def predict_basic(self):
        print((self.df.pred_Kw == self.df.real).mean())
        print((self.df.pred_Kw_ft == self.df.real).mean())
        p1 = [x['pred_Kw'] if x['confidence_Kw'] > x['confidence_Kw_ft'] else x['pred_Kw_ft'] for y, x in self.df.iterrows()]
        print((p1 == self.df.real).mean())
        return p1

    def predict_lr(self):
        models = {}
        df_res = pd.DataFrame()
        for n_fold, split in enumerate(self.splits):
            df_train, df_val = split
            y_train = df_train['real']
            y_val = df_val['real']
            fts = [col for col in df_train if col.startswith(tuple(['pred_' + x.lower() for x in CLASSES]))]
            X_train = df_train[fts]
            X_val = df_val[fts]

            model = LogisticRegression(random_state=0, solver='lbfgs', multi_class='multinomial')
            model.fit(X_train, y_train)
            models[n_fold] = model
            preds = model.predict(X_val)
            df_val['pred'] = preds
            df_res = df_res.append(df_val)

        df_res['correct'] = df_res.pred == df_res.real
        df_res.to_hdf(
            os.path.join(ROOT_PATH, 'data/preds/meta_lr.h5'),
            'meta_lr')
        print(df_res.correct.mean())

    def predict_lr_bin(self):
        models = {}
        df_res = pd.DataFrame()
        for n_fold, split in enumerate(self.splits):
            df_train, df_val = split
            y_train = df_train['real']
            y_val = df_val['real']
            fts = [col for col in df_train if col.startswith(tuple(['pred_' + x.lower() for x in CLASSES]))]
            X_train = df_train[fts]
            X_val = df_val[fts]

            model = LogisticRegression(random_state=0, solver='lbfgs', multi_class='multinomial')
            model.fit(X_train, y_train)
            models[n_fold] = model
            preds = model.predict(X_val)
            df_val['pred'] = preds
            df_res = df_res.append(df_val)

        df_res['correct'] = df_res.pred == df_res.real
        df_res.to_hdf(
            os.path.join(ROOT_PATH, 'data/preds/meta_lr.h5'),
            'meta_lr')
        print(df_res.correct.mean())

    def _create_splits(self, all_tabla):
        df = all_tabla.copy()
        df_image = df[['image','real']].drop_duplicates()
        kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=0)
        splits = list(kfold.split(X=df_image, y=df_image['real']))
        splits = [
            (df.merge(df_image.iloc[split[0]], on=['real', 'image']),
            df.merge(df_image.iloc[split[1]], on=['real', 'image']))
            for split in splits
        ]
        return splits

def main():
    # kw_model = KwokModel()
    # kw_model.fit()
    # kw_model.predict_validation()
    # kw_model.predict_last_layer()
    # df_test = kw_model.predict_test()
    # print(df_test)

    dkwf_model = DenseKwokFeatures()
    # dkwf_model.fit()
    dkwf_model.predict()

    # meta_model = MetaModel()
    # meta_model.predict_basic()



if __name__ == "__main__":
    main()
   
