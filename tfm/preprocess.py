from tfm import *
from pandas.api.types import CategoricalDtype


def create_patches(size_patch=1495, stride=99, resize_size=299):
    """Creates squared shaped patches from the original image

    :param size_patch: Side size of the patch, defaults to 1495
    :type size_patch: int, optional
    :param stride: Horizontal and vertical stride between patches, defaults to 99
    :type stride: int, optional
    :param resize_size: Side size of normalized patches, defaults to 299
    :type resize_size: int, optional
    """
    LOG.info("Creating patches...")
    path = os.path.join(ROOT_PATH, 'raw/labeled')
    path_patch = os.path.join(ROOT_PATH, 'data/patches')
    path_norm = os.path.join(ROOT_PATH, 'data/patches_norm')
    for name_class in CLASSES:
        LOG.debug(f"Creating {name_class} patches...")
        os.makedirs('/'.join([path_patch, name_class]), exist_ok=True)
        os.makedirs('/'.join([path_norm, name_class]), exist_ok=True)
        for filename in glob.glob('/'.join([path, name_class, '*.tif'])):
            name = filename.split('/')[-1].split('.')[0]
            LOG.debug(f"Creating {name} patch...")
            img = mpimg.imread(filename)
            list_patches=[]
            for dim in range(0,3):
                patches = view_as_windows(img[:,:,dim], (size_patch,size_patch),stride)
                last_col_patch = img[0:size_patch,-(size_patch+1):-1,dim]
                last_col_patch = np.reshape(last_col_patch,(1,size_patch,size_patch))

                patches=np.concatenate(patches[:][:])
                allpatches=np.concatenate((patches[:],last_col_patch),axis=0)

                patches = view_as_windows(img[-(size_patch+1):-1,:,dim], (size_patch,size_patch),stride)
                last_col_patch = img[-(size_patch+1):-1,-(size_patch+1):-1,dim]
                last_col_patch = np.reshape(last_col_patch,(1,size_patch,size_patch))

                patches=np.concatenate(patches[:][:])
                patches=np.concatenate((patches[:],last_col_patch),axis=0)
                allpatches = np.concatenate((allpatches,patches),axis=0)

                allpatches = np.reshape(allpatches,(1,14,size_patch,size_patch))
                list_patches.append(allpatches)

            patch_images=np.concatenate((list_patches[0],list_patches[1],list_patches[2]),axis=0)
            for patch in range(0,14):
                im = patch_images[:,patch,:,:]
                red=im[0,:,:]
                green=im[1,:,:]
                blue=im[2,:,:]
                red=np.reshape(red,(size_patch,size_patch,1))
                green=np.reshape(green,(size_patch,size_patch,1))
                blue=np.reshape(blue,(size_patch,size_patch,1))
                image=np.concatenate((red,green,blue),axis=2)
                imsave(os.path.join('/'.join([path_patch, name_class]), f'{name}patch{str(patch+1)}.tif'), image)
                re_pat = cv2.resize(image,(resize_size,resize_size))
                re_pat=np.uint8((255.*(re_pat - re_pat.min())/(re_pat.max() - re_pat.min())))
                imsave(os.path.join('/'.join([path_norm, name_class]), f'{name}patch{str(patch+1)}.tif'), re_pat)
    LOG.info("Patches created!")



def preprocesing():
    save_path = "data/daubechies/daubechieswavelet"
    read_path = "data/daubechies/predaubechies"

    os.makedirs(save_path, exist_ok=True)
    for filename in os.listdir(read_path):
        save_filename_path = '/'.join([save_path, filename])
        os.makedirs(save_filename_path, exist_ok=True)
        print(filename)
        read_filename_path = '/'.join([read_path, filename])
        print(read_filename_path)
        print(os.listdir(read_filename_path))
        for clas in os.listdir(read_filename_path):
            save_clas_path = '/'.join([save_filename_path, clas])
            os.makedirs(save_clas_path, exist_ok=True)

            print(clas)
            read_clas_path = '/'.join([read_filename_path, clas])

            for image in os.listdir(read_clas_path):
                save_image_path = '/'.join([save_clas_path, image])
                os.makedirs(save_image_path, exist_ok=True)

                read_image_path = '/'.join([read_clas_path, image])

                for wavelet in os.listdir(read_image_path):
                    read_wavelet_path = '/'.join([read_image_path, wavelet])
                    print(read_wavelet_path)
                    with open(read_wavelet_path, 'r') as myfile:
                        data = myfile.read().replace('i', 'j')

                    data = np.array([data])
                    save_wavelet_path = '/'.join([save_image_path, wavelet])
                    np.savetxt(save_wavelet_path[0:-4]+'.txt', data, fmt='%s')


def generate_tablas():

    read_path = 'data/tablas - features/isaac_features'
    save_path = 'data/tablas - features/isaac_features_tablas'
    file_names = ['im_isaac_wav_co_features.txt', 're_isaac_wav_co_features.txt']
    data_type = ['im', 're']
    angles = ['0', '45', '90', '135']
    ftrs = ['contrast', 'energy', 'entropy', 'lochom', 'maxprob', 'clsha', 'clprom', 'infocorr']

    os.makedirs(save_path, exist_ok=True)

    for n_dt in range(0,len(data_type)):
        print(data_type[n_dt])
        files = glob.glob('/'.join([read_path, '*/*/*/*', file_names[n_dt]]))

        df_paths = pd.DataFrame(data={'path':files})
        df_paths['clas'] = [x.split("\\")[1] for x in df_paths.path.values]
        df_paths['image'] = [x.split("\\")[2] for x in df_paths.path.values]
        df_paths['resolution'] = [x.split("\\")[3] for x in df_paths.path.values]
        df_paths['wavelet'] = [x.split("\\")[4][1] for x in df_paths.path.values]

        df_imgs = pd.DataFrame()
        cats_res = CategoricalDtype(categories=df_paths.resolution.unique())
        cats_clas = CategoricalDtype(categories=df_paths.clas.unique())
        cats_wav = CategoricalDtype(categories=df_paths.wavelet.unique())
        cats_im = CategoricalDtype(categories=df_paths.image.unique())
        df_imgs = pd.DataFrame()

        for row in df_paths.itertuples(index=False):
            data_dict = {}
            for n_angle in range(0,len(angles)):
                data = np.loadtxt(row.path, delimiter=' ')
                for n_ftr in range(0,len(ftrs)):
                    key = '_'.join([data_type[n_dt], angles[n_angle], ftrs[n_ftr]])
                    data_dict[key] = [float(data[n_ftr][n_angle])]

            data_dict['resolution'] = row.resolution
            data_dict['clase']= row.clas
            data_dict['image']= row.image
            data_dict['wavelet']= row.wavelet

            df_img = pd.DataFrame(data=data_dict)
            df_img['resolution'] = df_img.resolution.astype(cats_res)
            df_img['clase'] = df_img.clase.astype(cats_clas)
            df_img['image'] = df_img.image.astype(cats_im)
            df_img['wavelet'] = df_img.wavelet.astype(cats_wav)
            df_imgs = df_imgs.append(df_img)

        df_imgs.to_csv('/'.join([save_path, file_names[n_dt].split('.')[0]]), sep=';')
        print(file_names[n_dt].split('.')[0])


    ### im_isaac_wav_stat_features

    file_names = ['im_isaac_wav_stat_features.txt', 're_isaac_wav_stat_features.txt']
    ftrs = ['media', 'standard', 'skewness', 'maxim', 'minim']

    for n_dt in range(0,len(data_type)):
        files = glob.glob('/'.join([read_path, '*/*/*/*', file_names[n_dt]]))

        df_paths = pd.DataFrame(data={'path':files})
        df_paths['clas'] = [x.split("\\")[1] for x in df_paths.path.values]
        df_paths['image'] = [x.split("\\")[2] for x in df_paths.path.values]
        df_paths['resolution'] = [x.split("\\")[3] for x in df_paths.path.values]
        df_paths['wavelet'] = [x.split("\\")[4][1] for x in df_paths.path.values]

        df_imgs = pd.DataFrame()
        cats_res = CategoricalDtype(categories=df_paths.resolution.unique())
        cats_clas = CategoricalDtype(categories=df_paths.clas.unique())
        cats_wav = CategoricalDtype(categories=df_paths.wavelet.unique())
        cats_im = CategoricalDtype(categories=df_paths.image.unique())

        for row in df_paths.itertuples(index=False):
            data_dict = {}
            data = np.loadtxt(row.path, delimiter=' ')
            for n_ftr in range(0,len(ftrs)):
                key = '_'.join([data_type[n_dt], ftrs[n_ftr]])
                data_dict[key] = [float(data[n_ftr])]

            data_dict['resolution'] = row.resolution
            data_dict['clase']= row.clas
            data_dict['image']= row.image
            data_dict['wavelet']= row.wavelet

            df_img = pd.DataFrame(data=data_dict)
            df_img['resolution'] = df_img.resolution.astype(cats_res)
            df_img['clase'] = df_img.clase.astype(cats_clas)
            df_img['image'] = df_img.image.astype(cats_im)
            df_img['wavelet'] = df_img.wavelet.astype(cats_wav)

            df_imgs = df_imgs.append(df_img)

        df_imgs.to_csv('/'.join([save_path, file_names[n_dt].split('.')[0]]), sep=';')


    ### isaac_texture_variability

    files = glob.glob('/'.join([read_path, '*/*/*/*','isaac_texture_variability.txt']))
    df_paths = pd.DataFrame(data={'path':files})
    df_paths['clas'] = [x.split("\\")[1] for x in df_paths.path.values]
    df_paths['resolution'] = [x.split("\\")[3] for x in df_paths.path.values]
    df_paths['image'] = [x.split("\\")[2] for x in df_paths.path.values]
    df_paths['wavelet'] = [x.split("\\")[4][1] for x in df_paths.path.values]

    # num_img = 1# PORQUE?
    df_imgs = pd.DataFrame()
    cats_res = CategoricalDtype(categories=df_paths.resolution.unique())
    cats_clas = CategoricalDtype(categories=df_paths.clas.unique())
    cats_wav = CategoricalDtype(categories=df_paths.wavelet.unique())
    cats_im = CategoricalDtype(categories=df_paths.image.unique())

    for row in df_paths.itertuples(index=False):
        data = np.loadtxt(row.path, delimiter=' ')
        df_img = pd.DataFrame(data={
            'text_var_dx': [data[0]],
            'text_var_dy' : [data[1]],
            'resolution': row.resolution,
            'clase': row.clas,
            'image' : row.image,
            'wavelet': row.wavelet
            })

        df_img['text_var_dx'] = df_img.text_var_dx.astype(np.float32)
        df_img['text_var_dy'] = df_img.text_var_dy.astype(np.float32)
        df_img['resolution'] = df_img.resolution.astype(cats_res)
        df_img['clase'] = df_img.clase.astype(cats_clas)
        df_img['image'] = df_img.image.astype(cats_im)
        df_img['tipo_wavelet'] = df_img.wavelet.astype(cats_wav)
        df_imgs = df_imgs.append(df_img)

    df_imgs.to_csv('/'.join([save_path,'isaac_texture_variability']),sep=';')

    # ### Kwok predictions

    # df_imgs = pd.DataFrame()
    # df_img = pd.DataFrame(np.loadtxt('data/kwok/kwok_validation_predictions.txt', delimiter=' '))
    # df_imgs['prob_ben'] = df_img[:][0]
    # df_imgs['prob_situ'] = df_img[:][1]
    # df_imgs['prob_inva'] = df_img[:][2]
    # df_imgs['prob_nor'] = df_img[:][3]

    # names = pd.DataFrame(np.loadtxt('data/kwok/name_validation_kwok.txt', delimiter=' ',dtype=str))

    # df_imgs['image'] = names
    # df_imgs['image'] = df_imgs.image.astype(str)

    # df_imgs['prob_ben'] = df_imgs.prob_ben.astype(np.float32)
    # df_imgs['prob_situ'] = df_imgs.prob_situ.astype(np.float32)
    # df_imgs['prob_inva'] = df_imgs.prob_inva.astype(np.float32)
    # df_imgs['prob_nor'] = df_imgs.prob_nor.astype(np.float32)

    # df_imgs.to_csv('data/isaac_features_tablas_patches/kowk_validation_predictions',sep=';')


def start_tabla(tabla_path, name_path):
    final_tabla = pd.read_csv(tabla_path, delimiter=' ',
                          names=['prob_ben','prob_situ','prob_inva','prob_nor'])
    name_patches = pd.read_csv(name_path, delimiter=' ', names=['image'])
    name_patches.image = [x[:-4] for x in name_patches.image]
    final_tabla = pd.concat([final_tabla, name_patches], axis=1)
    final_tabla = final_tabla.set_index('image')

    return final_tabla

# def generate_final_tabla():

#     read_path = 'data/isaac_features_tablas_patches'
#     read_files = ['im_isaac_wav_co_features.csv', 're_isaac_wav_co_features.csv',
#                     'im_isaac_wav_stat_features.csv',
#                     're_isaac_wav_stat_features.csv',
#                     'isaac_texture_variability.csv']
#     kwok_pred = ['kwok_training_predictions.txt',
#                     'kwok_validation_predictions.txt', 'kwok_test_predictions.txt']
#     names = ['name_training.txt', 'name_validation_kwok.txt', 'name_test.txt']

#     final_tabla = pd.read_csv('data/kwok/kwok_training_predictions.txt',
#                                 delimiter=' ',
#                                 names=['prob_ben','prob_situ','prob_inva','prob_nor'])
#     name_patches = pd.read_csv('data/kwok/name_training.txt', delimiter=' ',
#                                 names=['image'])

#     final_tabla_train = start_tabla('/'.join(['data/kwok', kwok_pred[0]]),
#                                     '/'.join(['data/kwok', names[0]]))
#     final_tabla_val = start_tabla('/'.join(['data/kwok', kwok_pred[1]]),
#                                     '/'.join(['data/kwok', names[1]]))
#     final_tabla_test = start_tabla('/'.join(['data/kwok', kwok_pred[2]]),
#                                     '/'.join(['data/kwok', names[2]]))

#     for filename in read_files:
#         tabla = pd.read_csv('/'.join([read_path, filename]), delimiter=';')
#         if filename == 'isaac_texture_variability.csv':
#             tabla = tabla.drop(['Unnamed: 0', 'tipo_wavelet'], axis=1)
#         else:
#             tabla = tabla.drop(['Unnamed: 0'], axis=1)

#         tabla = tabla.pivot_table(index='image',
#                     columns=['resolution','wavelet'], values=tabla.columns[:-4])
#         tabla.columns = ['_'.join([x[1],x[2],x[0]]) for x in tabla.columns]

#         final_tabla_train = final_tabla_train.join(tabla)
#         final_tabla_val = final_tabla_val.join(tabla)
#         final_tabla_test = final_tabla_test.join(tabla)

#     n_train = len(final_tabla_train) / 4
#     n_val = len(final_tabla_val) / 4
#     n_test = len(final_tabla_test) / 4

#     final_tabla_train['clase'] = ['Benign'] * n_train + ['Insitu'] * n_train + ['Invasive'] * n_train + ['Normal'] * n_train
#     final_tabla_val['clase'] = ['Benign'] * n_val + ['Insitu'] * n_val + ['Invasive'] * n_val + ['Normal'] * n_val
#     final_tabla_test['clase'] = ['Benign'] * n_test + ['Insitu'] * n_test + ['Invasive'] * n_test + ['Normal'] * n_test

#     final_tabla_train.to_csv('D:/Master/Shanghai/tfm-bcn/data/final_tablas_patches/final_isaac_features_train.csv')
#     final_tabla_val.to_csv('D:/Master/Shanghai/tfm-bcn/data/final_tablas_patches/final_isaac_features_val.csv')
#     final_tabla_test.to_csv('D:/Master/Shanghai/tfm-bcn/data/final_tablas_patches/final_isaac_features_test.csv')

def generate_final_tabla():
    flag = 1
    read_path = os.path.join(ROOT_PATH, 'data/tablas - features/isaac_features_tablas')
    read_files = ['im_isaac_wav_co_features', 're_isaac_wav_co_features',
                    'im_isaac_wav_stat_features',
                    're_isaac_wav_stat_features']#,
    #                 'isaac_texture_variability']

    wavelet = ['H', 'V']
    resolution = ['1x','2x', '3x']

    for filename in read_files:
        tabla = pd.read_csv('/'.join([read_path, filename]), delimiter=';')
        if filename == 'isaac_texture_variability.csv':
            tabla = tabla.drop(['Unnamed: 0', 'tipo_wavelet'], axis=1)
        else:
            tabla = tabla.drop(['Unnamed: 0'], axis=1)

        tabla = tabla.set_index('image')
        
        if flag:
            image_names = tabla.index.unique()
            final_tabla = pd.DataFrame(index=image_names)
            flag=0
        
        name_columns = tabla.columns[:-3]
        sep_name_cols = list(name_columns.str.split('_'))
        sep_name_tabla = pd.DataFrame(sep_name_cols)
        
        if (np.shape(sep_name_tabla)[1] == 3):
            sep_name_tabla = pd.DataFrame(sep_name_cols, columns=['r_i','ºangle','features'])
        elif (np.shape(sep_name_tabla)[1] == 2):
            sep_name_tabla = pd.DataFrame(sep_name_cols, columns=['r_i','features'])
        
        re_or_im = sep_name_tabla.r_i.unique()[0]
        features = sep_name_tabla.features.unique()
        
        for string in features:
            lista = [string in x for x in sep_name_cols]
            for res in resolution:
                table_to_mean = tabla.loc[(tabla.resolution == res) & (tabla.wavelet.isin(wavelet)),lista]
                new_col = []
                for name in image_names:
                    temp = table_to_mean.loc[table_to_mean.index == name,:]
                    mean_value = temp.mean().mean()
                    new_col.append(mean_value)
                col_name = '_'.join([re_or_im,res,string])
                temp_tabla = pd.DataFrame(new_col,index=image_names,columns =[col_name])
                final_tabla = final_tabla.merge(temp_tabla, on='image')

                
    ## Here is just ofr texture varibility            
    last_file = 'isaac_texture_variability'
    tabla = pd.read_csv('/'.join([read_path, last_file]), delimiter=';')
    if filename == 'isaac_texture_variability.csv':
        tabla = tabla.drop(['Unnamed: 0', 'tipo_wavelet'], axis=1)
    else:
        tabla = tabla.drop(['Unnamed: 0'], axis=1)

    tabla = tabla.set_index('image')

    for res in resolution:
        table_to_mean = tabla.loc[(tabla.resolution == res) & (tabla.wavelet.isin(wavelet))].iloc[:,:-4]
        new_col = []
        for name in image_names:
            temp = table_to_mean.loc[table_to_mean.index == name,:]
            mean_value = temp.mean().mean()
            new_col.append(mean_value)
        col_name = '_'.join([res,'text_var'])
        temp_tabla = pd.DataFrame(new_col,index=image_names,columns =[col_name])
        
        final_tabla = final_tabla.merge(temp_tabla, on='image')

    # Don't forget to normalize and get rid of the inf values
    reduced_tabla = final_tabla.iloc[:,:-3]

    scaler = preprocessing.RobustScaler()
    norm_tabla = scaler.fit_transform(reduced_tabla)
    norm_tabla = pd.DataFrame(norm_tabla, columns = reduced_tabla.columns,index = reduced_tabla.index)

    # Add the real class prediction
    clases = ['Benign'] *100*14 + ['InSitu'] *100*14 + ['Invasive'] *100*14 + ['Normal'] *100*14
    real_tabla = pd.DataFrame(clases, index=final_tabla.index, columns=['real_class'])

    save_path = os.path.join(ROOT_PATH, 'data')

    final_tabla = final_tabla.merge(real_tabla, on='image')
    final_tabla.to_csv('/'.join([save_path,'final_tabla']), sep=';')

    norm_tabla = norm_tabla.merge(real_tabla, on='image')
    norm_tabla.to_csv('/'.join([save_path,'normalized_final_tablas']), sep=';')





def apply_radiomics():
    path = 'D:/Master/Shanghai/tfm-bcn/data/im_nrrd_norm_daubechies'
    features = ['original_firstorder_Minimum','original_firstorder_Maximum', 'original_firstorder_Mean', 'original_firstorder_Skewness',
            'original_glcm_ClusterProminence', 'original_glcm_ClusterShade', 'original_glcm_Contrast', 'original_glcm_MaximumProbability',
            'original_glcm_Correlation']
    masks_path = 'D:/Master/Shanghai/tfm-bcn/data/masks'
    clas_images_features = []
    clas_images = []
    # for clas in os.listdir(path):
    clas = os.listdir(path)[0]
    images = os.listdir('/'.join([path, clas]))
    for idx in range(0,len(images)): #range(0,len(all_images),14):
        image_features = []
        image = images[idx]
        print(image)
        for res in os.listdir('/'.join([path, clas, image])):
            if res == '1x':
                mask_filename = '/'.join([masks_path, '1x_mask.nrrd'])
            if res == '2x':
                mask_filename = '/'.join([masks_path, '2x_mask.nrrd'])
            if res == '3x':
                mask_filename = '/'.join([masks_path, '3x_mask.nrrd'])
            print(res)
            for wavelet in os.listdir('/'.join([path, clas, image, res])):
                data_path = '/'.join([path, clas, image, res, wavelet])
                featureVector = extractor.execute(data_path, mask_filename)
                for feature in features:
                    image_features.append(featureVector[feature])
        clas_images_features = np.array(clas_images_features)
        clas_images_features = np.vstack([clas_images_features, image_features]) if clas_images_features.size else image_features
        clas_images.append(image)

    tabla_features_radiomics = pd.DataFrame(clas_images_features)
    tabla_features_radiomics.index = clas_images

    resolutions = ['1x', '2x', '3x']
    wavelets = ['cD', 'cH', 'cV']
    col_names = ['Minimum','Maximum', 'Mean', 'Skewness', 'ClusterProminence', 'ClusterShade', 'Contrast',
                'MaximumProbability', 'Correlation']
    features = []

    for res in resolutions:
        for wav in wavelets:
            for feat in col_names:
                new_feature = '_'.join([res, wav, feat])
                features = np.array(features)
                features = np.vstack([features, new_feature]) if features.size else new_feature

    tabla_features_radiomics.columns = features


def main():
    pront('its ok')
    #preprocesing()
    generate_tablas()

if __name__ == "__main__":
    main()

